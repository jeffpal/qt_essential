#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QList<QMenu*> menus;
    menus = ui->menuBar->findChildren<QMenu*>();

    foreach (QMenu* menu, menus)
    {
        foreach (QAction* action, menu->actions())
        {
            if(action->isCheckable()){
            QMessageBox msgBox;
            msgBox.setText(menu->title()+" >> "+ action->iconText());
            action->setChecked(false);
            msgBox.exec();
            }
        }
    }
}
