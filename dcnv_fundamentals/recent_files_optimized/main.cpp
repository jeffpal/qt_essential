/*

.....................................................................
author: Jefferson Palheta
jeffersonpalheta.com
Copyright (C) 2018 Eng Jefferson Palheta. All rights reserved.
*/

#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCoreApplication::setApplicationName("OpenFiles");
    QCoreApplication::setOrganizationName("Jefferson");
    QCoreApplication::setOrganizationDomain("jeffersonpalheta.com");
    QCoreApplication::setApplicationVersion("v0.0.1");


    MainWindow openFileApp;
    openFileApp.show();



    return a.exec();
}
