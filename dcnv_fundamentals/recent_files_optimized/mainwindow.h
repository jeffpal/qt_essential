#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    const int max_recent_files_ = 5;
    QVector<QAction *> recent_files_actions_;

private slots:
    void on_push_button_open_file_clicked();

    void on_action_clear_recent_files_triggered();

private:
    void CreateActions();
    void CreateMenus();
    void OpenRecentFile();
    void UpdateRecentFiles();
    void UncheckMenuActions(QMenu *menu);
    void AddRecentFile(const QString &file_name);

    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
