#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSettings>
#include <QFileDialog>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    CreateActions();
    CreateMenus();
}

MainWindow::~MainWindow()
{
    while (recent_files_actions_.size() > 0) {
        delete recent_files_actions_.last();
        recent_files_actions_.pop_back();
    }
    delete ui;
}

void MainWindow::CreateActions() {
    // Recent files actions
    QAction *action;
    for (int i = 0; i < max_recent_files_; ++i) {
        action = new QAction(this);
        action->setVisible(false);
        connect(action, &QAction::triggered, this, &MainWindow::OpenRecentFile);
        recent_files_actions_.push_back(action);
    }
}

void MainWindow::CreateMenus() {
    // Populate recent files menu
    for (int i = 0; i < max_recent_files_; ++i)
        ui->menu_recent_files->insertAction(ui->action_clear_recent_files,
                                             recent_files_actions_[i]);

    ui->menu_recent_files->insertSeparator(ui->action_clear_recent_files);
    UpdateRecentFiles();
}

void MainWindow::OpenRecentFile() {
    QAction *action = qobject_cast<QAction *>(sender());
    if (action) {
        QString filename = action->data().toString();
        UncheckMenuActions(ui->menuItems);
        QMessageBox::information(this, "...", "The file loaded is:\n" + action->iconText());
    }
}

void MainWindow::UncheckMenuActions(QMenu *menu) {
    foreach (QAction *action, menu->actions()) {
        if (action->isCheckable()) action->setChecked(false);
    }
}

void MainWindow::UpdateRecentFiles() {
    QSettings settings;
    QStringList recent_files = settings.value("recent_files").toStringList();

    int number_recent_files = qMin(recent_files.size(), max_recent_files_);

    for (int i = 0; i < number_recent_files; ++i) {
        QString file_name = recent_files[i];
        QAction *action = recent_files_actions_[i];
        action->setText(file_name);
        action->setData(file_name);
        action->setVisible(true);
    }

    for (int i = number_recent_files; i < max_recent_files_; ++i)
        recent_files_actions_[i]->setVisible(false);

    ui->menu_recent_files->setEnabled(number_recent_files > 0);
}

void MainWindow::AddRecentFile(const QString &file_name) {
    if (file_name.isEmpty()) return;

    QSettings settings;
    QStringList recent_files = settings.value("recent_files").toStringList();

    //QString local_file_name = file_name;
    recent_files.removeAll(file_name);
    // inserts value at the beginning of the list
    recent_files.prepend(file_name);

    if (recent_files.length() > max_recent_files_) recent_files.removeLast();

    settings.setValue("recent_files", recent_files);

    UpdateRecentFiles();
}

void MainWindow::on_push_button_open_file_clicked()
{
    QString file_name = QFileDialog::getOpenFileName(this, "Open File");
    if(file_name.isEmpty()) return;
    AddRecentFile(file_name);
    QMessageBox::information(this, "...", "The file loaded is:\n" + file_name);
    UncheckMenuActions(ui->menuItems);
}

void MainWindow::on_action_clear_recent_files_triggered()
{
    QSettings settings;
    settings.remove("recent_files");

    for (int i = 0; i < max_recent_files_; ++i) recent_files_actions_[i]->setVisible(false);

    ui->menu_recent_files->setEnabled(false);
}
