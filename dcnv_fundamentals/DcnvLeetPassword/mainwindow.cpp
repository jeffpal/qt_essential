//Copyright (C) 2018 Deconve Technology. All rights reserved.

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->statusBar->showMessage("Copyright (C) 2018 Deconve Technology. All rights reserved.");

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_2_clicked()
{
    QString input = ui->inputPass->text();
    QString output;

    for(int i=0; i<input.size(); i++){

        if(input.at(i)=='i')
            output.append(QString::number(1));
        else if(input.at(i)=='z')
            output.append(QString::number(2));
        else if(input.at(i)=='e')
            output.append(QString::number(3));
        else if(input.at(i)=='A')
            output.append(QString::number(4));
        else if(input.at(i)=='s')
            output.append(QString::number(5));
        else if(input.at(i)=='b')
            output.append(QString::number(6));
        else if(input.at(i)=='t')
            output.append(QString::number(7));
        else if(input.at(i)=='T')
            output.append(QString::number(7));
        else if(input.at(i)=='B')
            output.append(QString::number(8));
        else if(input.at(i)=='g')
            output.append(QString::number(9));
        else if(input.at(i)=='o')
            output.append(QString::number(0));
        else if(input.at(i)=='O')
            output.append(QString::number(0));
        else if(input.at(i)=='a')
            output.append('@');
        else if(input.at(i)=='S')
            output.append('$');
        else
            output.append(input.at(i));
    }
    ui->textBrowser->setText(output);
}

void MainWindow::on_pushButton_clicked()
{
    QString input = ui->inputPass->text();
    QString output;

    for(int i=0; i<input.size(); i++){

        if(input.at(i)=='1')
            output.append('i');
        else if(input.at(i)=='2')
            output.append('z');
        else if(input.at(i)=='3')
            output.append('e');
        else if(input.at(i)=='4')
            output.append('A');
        else if(input.at(i)=='5')
            output.append('s');
        else if(input.at(i)=='6')
            output.append('b');
        else if(input.at(i)=='7')
            output.append('t');
        else if(input.at(i)=='7')
            output.append('T');
        else if(input.at(i)=='8')
            output.append('B');
        else if(input.at(i)=='9')
            output.append('g');
        else if(input.at(i)=='0')
            output.append('o');
        else if(input.at(i)=='0')
            output.append('O');
        else if(input.at(i)=='@')
            output.append('a');
        else if(input.at(i)=='$')
            output.append('S');
        else
            output.append(input.at(i));
    }
    ui->textBrowser->setText(output);
}
