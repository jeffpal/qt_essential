#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::UpdateRecentFiles(){
    QSettings settings;
    QStringList recentFiles = settings.value("recentFiles").toStringList();
//    qDebug() << "show" << recentFiles.length();
    if(recentFiles.length()>0){
        ui->actionClear_Recent_Files->setEnabled(true);
        clearRecentFilesActions();
        QActionGroup *files_action_group = new QActionGroup(this);
        files_action_group->setExclusive(true);

        foreach (const QString &file_name, recentFiles) {
            if(!file_name.isEmpty()){
                QAction *file_action = new QAction(file_name, files_action_group);
                ui->menuRecent_Files->addAction(file_action);
                connect(files_action_group, &QActionGroup::triggered, this, &MainWindow::useFile);
            }
        }
    }
    else
        QMessageBox::information(this, "...", "no actions");
}

void MainWindow::InsertRecentFile(){

    QString file_name = QFileDialog::getOpenFileName(this, "Open File");

    if (file_name.isEmpty()) return;

        QSettings settings;
        QStringList recent_files = settings.value("recentFiles").toStringList();

        short int maximum_files = 5;
        while(recent_files.length() >= maximum_files){
            recent_files.removeLast();
        }

        QString local_file_name = file_name;
        // To add a unique file to avoid repeated files (OPTIONAL)
        for (QStringList::iterator it = recent_files.begin(); it != recent_files.end(); ++it) {
            if (local_file_name.compare(*it) == 0) {
                recent_files.erase(it);
                break;
            }
        }
        recent_files.push_front(file_name); // equivalent recentFiles.insert(0, file_name);
        settings.setValue("recentFiles", recent_files);

        UpdateRecentFiles();

        ui->actionClear_Recent_Files->setEnabled(true);

}

void MainWindow::useFile(QAction *ac){

    QMessageBox::information(this, "...", ac->iconText());
}

void MainWindow::on_pushButtonOpenFile_clicked()
{
    MainWindow::InsertRecentFile();
}

void MainWindow::on_actionClear_Recent_Files_triggered()
{
    QSettings settings;
    settings.remove("recentFiles");
    clearRecentFilesActions();
    ui->actionClear_Recent_Files->setEnabled(false);
}

void MainWindow::clearRecentFilesActions(){

    foreach (QAction* action, ui->menuRecent_Files->actions()){
            if(action != ui->actionClear_Recent_Files)
                ui->menuRecent_Files->removeAction(action);
    }
}
