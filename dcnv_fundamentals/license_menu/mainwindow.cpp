#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "licensedialog.h"
#include "QLabel"
#include "QString"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionlicense_triggered()
{

    licensedialog dialog(this);
    dialog.setWindowTitle(tr("License"));
    dialog.SetTextLabel("Serial Number: xxxxxx\n Name: xxxxxx\nValidity:xx-xx-xx");

    dialog.exec();
}
