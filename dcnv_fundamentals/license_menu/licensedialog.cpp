#include "licensedialog.h"
#include "ui_licensedialog.h"
#include "QFileDialog"
#include "QLabel"
#include "QDebug"

licensedialog::licensedialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::licensedialog)
{
    ui->setupUi(this);
}

licensedialog::~licensedialog()
{
    delete ui;
}

void licensedialog::SetTextLabel(const QString &text){
    ui->label->setText(text);
    //ui->label->setGeometry(QRect(50, 20, 1000, 100));
}

void licensedialog::on_pushButton_clicked()
{

    QFileDialog fileDialog(this);
    fileDialog.setAcceptMode(QFileDialog::AcceptOpen);
    fileDialog.setWindowTitle(tr("Open License"));

    if (fileDialog.exec() == QDialog::Accepted) {
        const QString filename = fileDialog.selectedFiles().constFirst();
        qDebug() << filename;
        SetTextLabel("New license: "+filename);
}
}
