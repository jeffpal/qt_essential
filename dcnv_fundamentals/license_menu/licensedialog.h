#ifndef LICENSEDIALOG_H
#define LICENSEDIALOG_H

#include <QDialog>

namespace Ui {
class licensedialog;
}

class licensedialog : public QDialog
{
    Q_OBJECT

public:
    explicit licensedialog(QWidget *parent = 0);
    void SetTextLabel(const QString &text);
    ~licensedialog();

private slots:
    void on_pushButton_clicked();

private:
    Ui::licensedialog *ui;
};

#endif // LICENSEDIALOG_H
