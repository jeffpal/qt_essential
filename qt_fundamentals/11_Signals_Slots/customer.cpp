#include "customer.h"
#include <QDebug>

customer::customer(const QString &name, QObject *parent) : QObject(parent)
{
    customer_name_ = name;
}

customer::~customer(){

    qDebug() << "the " << customer_name_ << " object was deleted";
}
void customer::buy(const QString &product_name){

    emit bought(product_name);
}
