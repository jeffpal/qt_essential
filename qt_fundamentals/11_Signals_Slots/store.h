#ifndef STORE_H
#define STORE_H

#include <QObject>

class store : public QObject
{
    Q_OBJECT
public:
    explicit store(const QString &name, QObject *parent = nullptr);
    ~store();

signals:

public slots:
    void dispatcher(const QString &product_name);

private:
    QString store_name_;
};

#endif // STORE_H
