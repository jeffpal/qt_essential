#include <QCoreApplication>
#include <QObject>
#include "store.h"
#include "customer.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    store *amazon = new store("amazon");
    customer *jeff =  new customer("jefferson", amazon);

    QObject::connect(jeff, SIGNAL(bought(QString)), amazon, SLOT(dispatcher(QString)));
    jeff->buy("o poder do habito");

    delete amazon;

    return a.exec();
}
