#include "store.h"
#include <QDebug>

store::store(const QString &name, QObject *parent) : QObject(parent)
{
    store_name_ = name;
}
store::~store(){

    qDebug() << "The " << store_name_ << " object was deleted";
}
void store::dispatcher(const QString &product_name){

    qDebug() << "The product " << product_name << "is on the way";
}
