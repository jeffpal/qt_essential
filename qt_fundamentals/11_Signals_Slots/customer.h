#ifndef CUSTOMER_H
#define CUSTOMER_H

#include <QObject>

class customer : public QObject
{
    Q_OBJECT
public:
    explicit customer(const QString &name, QObject *parent = nullptr);
    ~customer();
    void buy(const QString &product_name);

signals:
    void bought(QString);

public slots:

private:
    QString customer_name_;
};

#endif // CUSTOMER_H
