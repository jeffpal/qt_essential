#include <QCoreApplication>
#include <QMap>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QMap<int, int> my_map;

    //notation: map[key] = value
    ++my_map[0];
    ++my_map[0];
    my_map[0]=8;

    ++my_map[114];
    ++my_map[114];

    qDebug() << "key of value 8: " << my_map.key(8); //key of value 8
    qDebug() << "value of key 144: " << my_map[114] << "\n";   //value of key 4

    int key;
    for(int i=0; i<=10; i++){
        key = qrand();
        my_map[key] = i;
    }

    qDebug() << "key: value";
    for(QMap<int, int>::iterator it = my_map.begin(); it != my_map.end(); it++){
        qDebug() << it.key() << ": " << it.value();
    }

    qDebug() << "\nchanging values\n";
    for(QMap<int, int>::iterator it = my_map.begin(); it != my_map.end(); it++){
        *it=0;
    }
    qDebug() << "key: value";
    for(QMap<int, int>::iterator it = my_map.begin(); it != my_map.end(); it++){
        qDebug() << it.key() << ": " << it.value();
    }

    return a.exec();
}
