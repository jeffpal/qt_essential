/*

.....................................................................
author: Jefferson Palheta
jeffersonpalheta.com
Copyright (C) 2018 Eng Jefferson Palheta. All rights reserved.
*/

#include <QCoreApplication>
#include <QVariant>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QVariant v;
    v.setValue<int>(5);
    double x = v.value<double>();
    qDebug() << v;
    qDebug() << x;

    QList<QVariant> vl;

    vl << 1 << 0.3 << "string";
    qDebug() << vl;

    return a.exec();
}
