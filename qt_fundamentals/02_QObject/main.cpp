/*
This code shows an inheritance case using QObject.

.....................................................................
author: Jefferson Palheta
jeffersonpalheta.com
Copyright (C) 2018 Eng Jefferson Palheta. All rights reserved.
*/

#include <QCoreApplication>
#include <QDebug>

class Person : public QObject{

public:
    Person(const QString &name, QObject *father = NULL)
        : QObject(father), name_(name) //we may use name_{ value } or name_( value ) to set its value.
    {

    }

    ~Person()
    {
        qDebug() << name_ << "was eliminated";
    }
    void SetName(const QString &name)
    {
        name_ = name;
    }

    void SetAge(int age)
    {
        age_ = age;
    }

    QString GetName() const
    {
        return name_;
    }

    int GetAge() const
    {
        return age_;
    }


private:
    QString name_;
    int age_;
};

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Person *father = new Person("Duke Philip");
    Person *child = new Person("Prince Charles", father);
    Person *grandchild = new Person("Prince William", child);
    grandchild->SetAge(25);
    qDebug() << "Grandchild name: " << grandchild->GetName() << "\nAge: " << grandchild->GetAge();
    delete father; //when the upper father is deleted its all children are deleted too
    return a.exec();
}
