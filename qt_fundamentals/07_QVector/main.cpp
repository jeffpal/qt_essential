/*

TODO: take a look at QVector vs QList matter.

.....................................................................
author: Jefferson Palheta
jeffersonpalheta.com
Copyright (C) 2018 Eng Jefferson Palheta. All rights reserved.
*/

#include <QCoreApplication>
#include <QVector>
#include <QDebug>


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QVector<QString> items;

    items << "item 0" << "item 1" << "item2";
    qDebug() << "Complete Vector\n:" << items;

    qDebug() << "\nEach item:";
    foreach(const QString &item, items){
        qDebug() << item;
    }

    return a.exec();
}
