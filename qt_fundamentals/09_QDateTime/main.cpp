#include <QCoreApplication>
#include <QDateTime>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QDate my_date(2018, 3, 25);
    QTime my_time(22, 12, 00);
    QDateTime my_date_time(my_date, my_time);
    qDebug() << "my date: " << my_date_time;

    QDateTime auto_date_time = QDateTime::currentDateTime();
    qDebug() << "\nauto date: " << auto_date_time;
    return a.exec();
}
