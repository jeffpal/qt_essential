#include <QCoreApplication>
#include <QDebug>
#include <iostream>


using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QString qs1 = "Sergio";
    QString qs2 = "Marquina";
    QString qs3;

    string s1 = qs1.toStdString();
    string s2 = qs2.toStdString();
    string s3 = s1 + " " + s2;

    cout << "Converting QString to Standard String and printing string with 'cout'" << endl;
    cout << s3 << endl << endl;


    qs1.clear();
    qs2.clear();
    s1.clear();
    s2.clear();
    s3.clear();

    s1 = "I love";
    s2 = "cpp";
    qs1 = s1.c_str();
    qs2 = s2.c_str();
    qs3 = qs1 + " " + qs2;

    //qDebug() do not accept string std. We need to convert it using .c_str()
    qDebug() << "Converting Standard String to QString and printing string with 'qDebug()'";
    qDebug() << qs3;
    qs3.clear();

    int x = 101;
    float y = 3.14;
    QString z = "jeff";

    qs3 = QString("%1 %2 %3").arg(x).arg(y).arg(z);
    qDebug() << qs3 << "\n";

    bool contains = qs3.contains("jeff");
    qDebug() << "contains: " << contains;

    int index = qs3.indexOf("jeff");
    qDebug() << "index: " << index << "\n";

    qDebug() << "qs3 at 8: " << qs3.at(8) << "\n";

    qDebug() << "how many time 1 appears: " << qs3.count("1") << "\n";

    qDebug() << "s3 starts with 'jeff': " << qs3.startsWith("jeff");

    qDebug() << "s3 ends with 'jeff': " << qs3.endsWith("jeff") << "\n";

    qs3.insert(0, "_insert_");
    qs3.append("_append_");
    qs3.prepend(" prepend ");
    qs3.push_back("_push_back_");
    qs3.push_front("_push_front_");
    qDebug() << qs3;

    return a.exec();
}
