/*

.....................................................................
author: Jefferson Palheta
jeffersonpalheta.com
Copyright (C) 2018 Eng Jefferson Palheta. All rights reserved.
*/

#include <QCoreApplication>
#include <QTextStream>
#include <QFile>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QString file_content;
    QTextStream io;
    QFile file("./io.txt");

    file.open(QIODevice::ReadWrite | QIODevice::Text);
    if(!file.isOpen())
    {
        qDebug() << "It was not possible to open the file";
    }
    io.setDevice(&file);
    io << "mornings are for coffee and contemplation";
    file.flush();
    file.close();

    file.open(QIODevice::ReadWrite | QIODevice::Text);
    if(!file.isOpen())
    {
        qDebug() << "It was not possible to open the file";
    }
    io.setDevice(&file);
    file_content = io.readAll();
    qDebug() << file_content;
    return a.exec();
}
