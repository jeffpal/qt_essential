/*

.....................................................................
author: Jefferson Palheta
jeffersonpalheta.com
Copyright (C) 2018 Eng Jefferson Palheta. All rights reserved.
*/

#include <QCoreApplication>
#include <QList>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    //QList<QString> is equal to QStringList
    QList<QString> items;

    items << "entry 0" << "entry 1" << "entry 2";

    qDebug() << "way 1 to go through list";
    for(int i = 0; i<items.length(); i++) qDebug() << items.at(i);

    qDebug() << "\nway 2 to go through list";
    for(QList<QString>::iterator i = items.begin(); i != items.end(); i++) qDebug() << *i;

    qDebug() << "\nway 3 to go through list";
    foreach (const QString &item, items) {
        qDebug() << item;
    }

    qDebug() << "\nComplete List:\n" << items;

    QString text =  "I am brazilian";
    QStringList text_split= text.split(" ");
    QString text_join = text_split.join(" ");

    qDebug() << "\nOriginal text:\n" << text;
    qDebug() << "\nText to list:\n" << text_split;
    qDebug() << "\nList to text:\n" << text_join;
    return a.exec();
}
