/*
This code set a path and show all directories and files presented
into that.

.....................................................................
author: Jefferson Palheta
jeffersonpalheta.com
Copyright (C) 2018 Eng Jefferson Palheta. All rights reserved.
*/

#include <QCoreApplication>
#include <QDir>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QDir directory;
    directory.setPath("/home/jefferson");

    int dirs = directory.count();
    for (int i=0; i<=dirs; i++)
    {
        qDebug() << directory[i];
    }
    return a.exec();
}
