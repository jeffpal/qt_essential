/*
This code read a file from project source directory and write another one
with the same content in the current directory(after build it'll be build directory).

.....................................................................
author: Jefferson Palheta
jeffersonpalheta.com
Copyright (C) 2018 Eng Jefferson Palheta. All rights reserved.
*/

#include <QCoreApplication>
#include <QFile>
#include <QDebug>
#include <QDir>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QFile input_file(":data/input.md");
    /*
      for read files from our current project directory
      instead of build dir (the current directory after build)
      we need to add a Resources dir (RESOURCES += ./) in the .pro,
      with our project directory (./). And use the character : before
      the file's name.
    */

    if(!input_file.exists()){
        qDebug() << "The file doesn't exist";
    }
    input_file.open(QIODevice::ReadOnly | QIODevice::Text);
    if(!input_file.isOpen()){
        qDebug() << "Was not possible to open the input file";
    }
    QByteArray input_file_content;
    input_file_content = input_file.readAll();
    input_file.close();

    QFile output_file("./output.md");

    output_file.open(QIODevice::WriteOnly | QIODevice::Text);
    if(!output_file.isOpen()){
        qDebug() << "Was not possible to open or create the output file";
    }
    output_file.write(input_file_content);
    output_file.close();

    QFileInfo input_file_info(":data/input.md");
    qDebug() << "FILE SIZE: " << input_file_info.size() << "Bytes";
    qDebug() << "THE INPUT FILE CONTENT IS: \n\n" << input_file_content;
    return a.exec();
}
